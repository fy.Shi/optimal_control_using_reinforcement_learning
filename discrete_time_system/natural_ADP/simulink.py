
import numpy as np

class simulink:
    def __init__(self):
        self.state_dim, self.action_dim = 2, 1

    def reset(self):
        self.x = np.array([1., 0.])
        return self.x
    def step(self, u):
        self.x[0] = self.x[0] + self.x[1]
        self.x[1] = self.x[1] + u
        reward = self.x[0]*self.x[0] + 2*self.x[0]*self.x[1] + 4*self.x[1]*self.x[1] + u*u
        return self.x, reward

# class simulink:
#     def __init__(self):
#         self.state_dim, self.action_dim = 2, 1
#         self.A = np.array([[0.9974, 0.0539], [-0.1078, 1.1591]])
#         self.B = np.array([0.0013, 0.0539])
#
#     def reset(self):
#         self.x = np.array([2., 1.])
#         return self.x
#
#     def step(self, u):
#         reward = 0.5*np.dot( self.x, np.dot( np.array([[0.25, 0], [0, 0.05]]), self.x ) ) + 0.05*u*u
#         self.x = np.dot(self.A, self.x) + np.dot(self.B, u)
#         return self.x, reward
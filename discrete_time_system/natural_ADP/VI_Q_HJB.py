import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable

import numpy as np
import matplotlib.pyplot as plt

from simulink import *

step_number = 200

class Net(nn.Module):
    """docstring for Net"""
    def __init__(self, state_dim, action_dim):
        super(Net, self).__init__()
        self.fc1_s = nn.Linear(state_dim, 50)
        self.fc1_s.weight.data.normal_(0,0.1)
        self.fc1_a = nn.Linear(action_dim, 50)
        self.fc1_a.weight.data.normal_(0, 0.1)
        self.fc2 = nn.Linear(50,50)
        self.fc2.weight.data.normal_(0,0.1)
        self.out = nn.Linear(50,1)
        self.out.weight.data.normal_(0,0.1)

    def forward(self, state, action):
        # state = Variable(torch.from_numpy(state), requires_grad=True)
        # state = torch.FloatTensor(state)
        # action = torch.FloatTensor(action.reshape((len(action), 1)))
        x_s = self.fc1_s(state)
        x_s = torch.tanh(x_s)
        x_a = self.fc1_a(action)
        x_a = torch.tanh(x_a)
        x = x_a + x_s
        x = self.fc2(x)
        x = torch.tanh(x)
        Q_value = self.out(x)
        return Q_value

class QLearning:
    def __init__(self, state_dim, action_dim, gamma, learning_rate):
        self.state_dim, self.action_dim = state_dim, action_dim
        self.gamma, self.learning_rate = gamma, learning_rate
        self.q_value = Net(state_dim=state_dim, action_dim=action_dim)
        self.optimizer = torch.optim.Adam(self.q_value.parameters(), lr=self.learning_rate)
        self.loss_func = nn.MSELoss()

    def update_net(self, state_list, action_list, reward_list):
        reward_list = np.array(reward_list).reshape((len(reward_list), 1))
        q_eval = self.q_value.forward(state=torch.FloatTensor(state_list[0:-2]), action=torch.FloatTensor(action_list[0:len(state_list[0:-2])]).resize_((len(state_list[0:-2]), 1)))
        q_next = self.q_value.forward(state=torch.FloatTensor(state_list[1:-1]), action=torch.FloatTensor(action_list[1:1+len(state_list[0:-2])]).resize_((len(state_list[0:-2]), 1)))
        gamma_q_target = self.gamma * q_next
        target_reward_list = torch.FloatTensor(reward_list[0:len(state_list[0:-2])])
        q_target = target_reward_list + gamma_q_target
        q_target_numpy = q_target.detach().numpy()
        # q_target_numpy = np.append(q_target_numpy, reward_list[-1])
        q_target_tensor = torch.FloatTensor(q_target_numpy.reshape((len(q_target_numpy), 1)))
        loss = self.loss_func(q_eval, q_target_tensor)
        self.optimizer.zero_grad()
        loss.backward()
        self.optimizer.step()
        return loss.detach().numpy()

    def get_grad(self, x, u):
        x = Variable(torch.Tensor(np.array(x).reshape((1, self.state_dim))), requires_grad=True)
        u = torch.Tensor(np.array(u).reshape((1, self.action_dim)))
        q_value = self.q_value.forward(state=x,
                                       action=u)
        q_value.backward()
        return x.grad

def get_optimal_policy():
    env = simulink()
    x = env.reset()
    sumreward = 0.
    x_1_list, x_2_list, u_list = list(), list(), list()
    x_1_list.append(x[0])
    x_2_list.append(x[1])
    for _ in range(step_number):
        u = -x[0] - np.sqrt(6) * x[1]
        x, reward = env.step(u)
        x_1_list.append(x[0])
        x_2_list.append(x[1])
        u_list.append(u)
    return np.array(u_list)

def get_state_vector_and_sum_cost(control_list):
    env = simulink()
    x = env.reset()
    sum_cost = 0.0
    state_list, reward_list = np.empty((len(control_list)+1, env.state_dim)), list()
    for i in range(len(control_list)):
        state_list[i] = x
        x, cost = env.step(control_list[i])
        reward_list.append(cost)
        sum_cost += cost
    state_list[len(control_list)] = x
    return sum_cost, state_list, reward_list

def plot_state_and_policy(policy, iter_num):
    env = simulink()
    x = env.reset()
    sumreward = 0.
    x_1_list, x_2_list, u_list, reward_list = list(), list(), list(), list()
    x_1_list.append(x[0])
    x_2_list.append(x[1])
    for u in policy:
        x, reward = env.step(u)
        reward_list.append(reward)
        x_1_list.append(x[0])
        x_2_list.append(x[1])
        u_list.append(u)
        sumreward += reward

    fig = plt.figure()

    ax = plt.subplot("221")
    ax.set_title("state $x_1$")
    ax.plot(x_1_list)

    ax = plt.subplot("222")
    ax.set_title("state $x_2$")
    ax.plot(x_2_list)

    ax = plt.subplot("223")
    ax.set_title("u")
    ax.plot(u_list)

    ax = plt.subplot("224")
    ax.set_title("cost - sum cost = {}".format(sumreward))
    ax.plot(reward_list)

    plt.savefig("./figs/"+str(iter_num)+".png")
    plt.close()
    # plt.show()

def train():
    QL = QLearning(state_dim=2, action_dim=1, gamma=0.99, learning_rate=1e-3)
    error, policy = \
        1e10, np.zeros(step_number, dtype=np.float)
    # policy = get_optimal_policy()
    policy = np.random.uniform(low=-1.0, high=1.0, size=step_number)
    sum_cost_, state_list, reward_list = get_state_vector_and_sum_cost(policy)
    eposide = 0
    plot_state_and_policy(policy, iter_num=eposide)
    while error > 1e-10:
        eposide += 1
        loss = 100
        # while 1e-2 < loss:
        for _ in range(1):
            loss = QL.update_net(state_list=state_list, action_list=policy, reward_list=reward_list)
            print('prroximate loss: ', loss)
        for i in range(len(policy)):
            grad = QL.get_grad(state_list[i], policy[i]).resize_(QL.state_dim).numpy()
            policy[i] = -10*np.dot(np.array([0.0013, 0.0539]), grad)
        if 0 == eposide%100:
            plot_state_and_policy(policy, iter_num=eposide)
        sum_cost, state_list, reward_list = get_state_vector_and_sum_cost(policy)
        error = np.abs(sum_cost-sum_cost_)
        sum_cost_ = sum_cost
        # print('cost error: ', error, ' | sum cost: ', sum_cost)
    return 0

if __name__ == '__main__':
    train()


#
# net = Net(state_dim=2, action_dim=1)
# x = Variable(torch.Tensor([1.,1.]), requires_grad=True)
# u = Variable(torch.Tensor([1.418]), requires_grad=False)
# q_value = net.forward(state=x, action=u)
# q_value.backward()
# print(x.grad)

# net = Net1()
# x = Variable(torch.Tensor([1.,1.]), requires_grad=True)
# q_value = net(x)
# # torch.Tensor([1., 1.])
# q_value.backward(gradient=None)
# print(x.grad)
# y = Variable(torch.Tensor([1.,1.]), requires_grad=True)
# q_value_y = net(y)
# # torch.Tensor([1., 1.])
# q_value_y.backward(gradient=None)
# print(y.grad)
# y1 = Variable(torch.Tensor([200.,200.]), requires_grad=True)
# q_value_y1 = net(y1)
# # torch.Tensor([1., 1.])
# q_value_y1.backward(gradient=None)
# print(y1.grad)







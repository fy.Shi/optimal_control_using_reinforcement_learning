import matplotlib.pyplot as plt
import numpy as np
from simulink import *

env = simulink()
x = env.reset()
sumreward = 0.
x_1_list, x_2_list, u_list, reward_list = list(), list(), list(), list()
x_1_list.append(x[0])
x_2_list.append(x[1])
for _ in range(100):
    u = -x[0]-np.sqrt(6)*x[1]
    x, reward = env.step(u)
    reward_list.append(reward)
    x_1_list.append(x[0])
    x_2_list.append(x[1])
    u_list.append(u)
    sumreward += reward

fig = plt.figure()

ax = plt.subplot("221")
ax.set_title("state $x_1$")
ax.plot(x_1_list)

ax = plt.subplot("222")
ax.set_title("state $x_2$")
ax.plot(x_2_list)

ax = plt.subplot("212")
ax.set_title("control curve, sum cost = {}".format(sumreward))
ax.plot(u_list)

plt.show()
